package ru.t1k.vbelkin.tm.api;

import ru.t1k.vbelkin.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

    void showProject(Project project);

    void showProjectById();

    void showProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();
}
