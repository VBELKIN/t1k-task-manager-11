package ru.t1k.vbelkin.tm.api;

import ru.t1k.vbelkin.tm.model.Task;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void showTask(Task task);

    void showTaskById();

    void showTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
