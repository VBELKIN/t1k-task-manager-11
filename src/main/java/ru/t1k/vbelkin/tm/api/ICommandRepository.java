package ru.t1k.vbelkin.tm.api;

import ru.t1k.vbelkin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
