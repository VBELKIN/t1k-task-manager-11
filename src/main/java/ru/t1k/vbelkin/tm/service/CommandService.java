package ru.t1k.vbelkin.tm.service;

import ru.t1k.vbelkin.tm.api.ICommandRepository;
import ru.t1k.vbelkin.tm.api.ICommandService;
import ru.t1k.vbelkin.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
