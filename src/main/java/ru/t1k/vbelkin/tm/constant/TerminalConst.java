package ru.t1k.vbelkin.tm.constant;

public final class TerminalConst {
    public final static String VERSION = "version";
    public final static String HELP = "help";
    public final static String ABOUT = "about";
    public final static String INFO = "info";
    public final static String EXIT = "exit";
    public final static String ARGUMENTS = "arguments";
    public final static String COMMANDS = "commands";
    public final static String PROJECT_CLEAR = "project-clear";
    public final static String PROJECT_LIST = "project-list";
    public final static String PROJECT_CREATE = "project-create";
    public final static String TASK_CLEAR = "task-clear";
    public final static String TASK_LIST = "task-list";
    public final static String TASK_CREATE = "task-create";
    public final static String TASK_SHOW_BY_ID = "task-show-by-id";
    public final static String TASK_SHOW_BY_INDEX = "task-show-by-index";
    public final static String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public final static String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public final static String TASK_UPDATE_BY_ID = "task-update-by-id";
    public final static String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public final static String PROJECT_SHOW_BY_ID = "project-show-by-id";
    public final static String PROJECT_SHOW_BY_INDEX = "project-show-by-index";
    public final static String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public final static String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public final static String PROJECT_UPDATE_BY_ID = "project-update-by-id";
    public final static String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
}
